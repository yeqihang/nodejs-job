//1.1 处理中文乱码
//1.2 处理URL路径信息（首页，登入，注册，产品列表，订单列表）
const { read } = require('fs');
let http = require('http');
let server = http.createServer((req,res) => {
    if (req.url == '/favicon.ico') {
        return;
    }
    res.setHeader('Content-Type','text/html; charset=utf-8');
    if (req.url=='/index/index' || req.url == '/') {
        res.end('首页');
    }else if (req.url == '/user/login') {
        res.end('登录');
    }else if (req.url == '/user/reg') {
        res.end('注册');
    }else if (req.url == '/list/order') {
        res.end('订单列表');
    }else if (req.url == '/list/product') {
        res.end('产品列表');
    }else{
        res.end('404 无法找到该页面');
    }
   
    
})
server.listen(81,'yqh123456.com',()=>{
    console.log('yqh123456.com 创建成功！');
});