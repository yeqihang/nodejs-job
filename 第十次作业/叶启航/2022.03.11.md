## 一、MVC4

###### 1、post

```
//获取POST数据
    GetPostData(callBack){
        let allData = '';
        //数据到来事件处理
        this.request.req.on('data',(data)=>{
            allData+=data;
        });
        //数据发送结束事件处理
        this.request.req.on('end',()=>{
            //这里面是个人做个人的，别的都一样。
            callBack(allData);//回调函数
        });
    }
```

```
this.GetPostData((data)=>{
                //1、做业务逻辑处理
                //....
                console.log(data);
                //2、响应客户端
                this.res.end('执行用户注册操作');
            });
```

服务器一定要用end结束。